package net.proloquor.prolog.lambda;

public class SimpleMathOperations implements MathOperations {
    @Override
    public int add(int a, int b) {
        return a + b;
    }

    @Override
    public int subtract(int a, int b) {
        return a - b;
    }

    @Override
    public int multiply(int a, int b) {
        return a * b;
    }

    @Override
    public int divide(int a, int b) throws IllegalArgumentException {
        if(b == 0) {
            throw new IllegalArgumentException();
        }
        return a / b;
    }
}
