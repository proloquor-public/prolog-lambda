package net.proloquor.prolog.lambda;

public interface MathOperation {
    int operation(int a, int b);
}
