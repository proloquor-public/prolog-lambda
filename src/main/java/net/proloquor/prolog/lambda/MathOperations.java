package net.proloquor.prolog.lambda;

public interface MathOperations {
    int add(int a, int b);
    int subtract(int a, int b);
    int multiply(int a, int b);
    int divide(int a, int b) throws IllegalArgumentException;
}
