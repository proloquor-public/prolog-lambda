package net.proloquor.prolog.lambda;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleMathOperationsTest {

    SimpleMathOperations operations;

    @BeforeEach
    void setUp() {
        operations = new SimpleMathOperations();
    }

    @Test
    void add() {
        assertEquals(15, operations.add(10, 5));
    }

    @Test
    void subtract() {
        assertEquals(5, operations.subtract(10, 5));
    }

    @Test
    void multiply() {
        assertEquals(50, operations.multiply(10, 5));
    }

    @Test
    void divide() {
        assertEquals(2, operations.divide(10, 5));
    }

    @Test
    void divideByZero() {
        assertThrows(IllegalArgumentException.class,
                () -> operations.divide(10, 0));
    }
}