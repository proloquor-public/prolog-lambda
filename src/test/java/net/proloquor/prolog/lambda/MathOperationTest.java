package net.proloquor.prolog.lambda;

import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class MathOperationTest {

    MathOperation add = (int a, int b) -> a + b;
    MathOperation subtract = (int a, int b) -> a - b;
    MathOperation multiply = (int a, int b) -> a * b;
    MathOperation divide = (int a, int b) -> a / b;
    MathOperation power = (int a, int b) -> (int)Math.pow(a, b);

    @Test
    void operation() {
        assertEquals(15, add.operation(10, 5));
        assertEquals(5, subtract.operation(10, 5));
        assertEquals(50, multiply.operation(10, 5));
        assertEquals(2, divide.operation(10, 5));
        assertEquals(100000, power.operation(10, 5));
    }

    @Test
    void operateTest() {

        /* Example of 'divide', both successful and with an IllegalArgument. */
        assertEquals(2, operate(10, 5, (int a, int b) -> a / b));
        assertThrows(IllegalArgumentException.class,
                () -> operate(10, 0, (int a, int b) -> a / b));

        /* Example of a min(a, b) function. */
        assertEquals(5, operate(10, 5, (int a, int b) -> (a < b) ? a : b));

        /* Example of a Combination (nCr) function. */
        assertEquals (252, operate(10, 5,
                (int a, int b) -> (int) (factorial(a) / factorial(b) / factorial(a - b))));
    }

    private int operate(int a, int b, MathOperation op) {
        try {
            return op.operation(a, b);
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    private long factorial(int n) {
        return IntStream.rangeClosed(1, n).reduce(1, (int x, int y) -> x * y);
    }
}